package main

import (
	"net/http"
	"io/ioutil"
)


func main() {
	resp, err := http.Get("https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10-million-password-list-top-1000000.txt")
	if err != nil {
		panic(err)
	}

	passwords, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	ioutil.WriteFile("passwords.txt", passwords, 0644)
}