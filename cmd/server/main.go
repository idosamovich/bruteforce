package main

import (
	"io/ioutil"
	"fmt"
	"net/http"
)

func indexPageHandler(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
	data, err := ioutil.ReadFile("index.html")
	if err != nil {
		panic(err)
	}

	w.Write(data)
}

func loginPageHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("username") != "admin" {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Only admin is allowed."))
		return
	}

	if r.FormValue("password") != "Password1" {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Wrong password."))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Logged in successfully."))
}

func main() {
	http.HandleFunc("/", indexPageHandler)
	http.HandleFunc("/login", loginPageHandler)

	fmt.Println(http.ListenAndServe(":8080", nil))
}