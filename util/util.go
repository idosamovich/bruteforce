package util

import (
	"io/ioutil"
	"strconv"
	"strings"
)

const (
	defaultNumberSuffix = "th"
	stNumberSuffix = "st"
	ndNumberSuffix = "nd"
	rdNumberSuffix = "rd"
)

func ReadPasswordsFile(passwordsFilePath string) ([]string, error) {
	rawFile, err := ioutil.ReadFile(passwordsFilePath)
	if err != nil {
		return nil, err
	}

	return strings.Split(string(rawFile), "\n"), nil
}


func CalcNumberSuffix(number int) string {
	strNumber := strconv.Itoa(number)
	if strings.HasSuffix(strNumber, "11") ||
	   strings.HasSuffix(strNumber, "12") ||
	   strings.HasSuffix(strNumber, "13") {
		return defaultNumberSuffix
	}

	if strings.HasSuffix(strNumber, "1") {
		return stNumberSuffix
	}

	if strings.HasSuffix(strNumber, "2") {
		return ndNumberSuffix
	}

	if strings.HasSuffix(strNumber, "3") {
		return rdNumberSuffix
	}

	return defaultNumberSuffix
}