package bruteforcer

import (
	"time"
	"fmt"
	"runtime"

	"ido/bruteforce/util"
	"github.com/gammazero/workerpool"
)

const (
	testcaseMessage = "testing %s. It is the %d%s password in the file.\n"	
)

type passwordBruteForcer struct {
	passwordsList []string
	testCaseFunc func(string) (bool, error)
	workerPool *workerpool.WorkerPool
}

func New(passwordsList []string, testCaseFunc func(string) (bool, error)) *passwordBruteForcer {
	return &passwordBruteForcer{
		passwordsList: passwordsList,
		testCaseFunc: testCaseFunc,
		workerPool: workerpool.New(runtime.NumCPU()),
	}
}

func (pbf *passwordBruteForcer) BruteForce() (string, error) {
	answerChan := make(chan string,1)
	errChan := make(chan error,1)
	for passwordNumber, password := range pbf.passwordsList {
		pbf.startWorker(password, passwordNumber+1, answerChan, errChan)
	}

	correctPassword := ""
	var err error
	for !pbf.workerPool.Stopped() {
		select {
		case correctPassword = <-answerChan:
			pbf.workerPool.Stop()
			break

		case err = <-errChan:
			pbf.workerPool.Stop()
			break

		default:
			if pbf.workerPool.WaitingQueueSize() == 0 {
				pbf.workerPool.StopWait()
			}
			time.Sleep(time.Second)
		}
	}

	return correctPassword, err
}


func (pbf *passwordBruteForcer) startWorker(password string, passwordNumber int, answerChan chan string, errChan chan error) {
	pbf.workerPool.Submit(func() {
		fmt.Printf(testcaseMessage, password, passwordNumber, util.CalcNumberSuffix(passwordNumber))
		if ok, err := pbf.testCaseFunc(password); ok {
			answerChan<-password
		} else if err != nil {
			errChan<-err
		}
	})
}